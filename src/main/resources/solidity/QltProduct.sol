pragma solidity ^0.5.7;

contract QltProduct{
	address owner = msg.sender;

	struct Review {
    		string id;
		string productId;
		string author;
		string text;
    		string date;
		string stars;
  	}
	Review[] reviews;
	
	event NewReview(string id, string productId, string author, string text, string date, string stars);

	
    //Constructor
    constructor(
	string memory _id,
	string memory _productId,
	string memory _author,
	string memory _text,
    	string memory _date,
	string memory _stars
        ) public
    {   
        reviews.push(Review(_id, _productId, _author, _text, _date, _stars));
	emit NewReview(_id, _productId, _author, _text, _date, _stars);
    }


    modifier checkOnlyOwner(address _account)
    {
        require(msg.sender == _account);
        _;
    }
    //add review
    function addReview(string memory id, string memory productId, string memory author, string memory text, string memory date, string memory stars) public
	checkOnlyOwner(owner)
  	{
	reviews.push(Review(id, productId, author, text, date, stars));
	emit NewReview(id, productId, author, text, date, stars);
     }
}