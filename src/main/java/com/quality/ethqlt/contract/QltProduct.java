package com.quality.ethqlt.contract;

import io.reactivex.Flowable;
import org.web3j.abi.EventEncoder;
import org.web3j.abi.FunctionEncoder;
import org.web3j.abi.TypeReference;
import org.web3j.abi.datatypes.Event;
import org.web3j.abi.datatypes.Function;
import org.web3j.abi.datatypes.Type;
import org.web3j.abi.datatypes.Utf8String;
import org.web3j.crypto.Credentials;
import org.web3j.protocol.Web3j;
import org.web3j.protocol.core.DefaultBlockParameter;
import org.web3j.protocol.core.RemoteCall;
import org.web3j.protocol.core.methods.request.EthFilter;
import org.web3j.protocol.core.methods.response.Log;
import org.web3j.protocol.core.methods.response.TransactionReceipt;
import org.web3j.tx.Contract;
import org.web3j.tx.TransactionManager;
import org.web3j.tx.gas.ContractGasProvider;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * <p>Auto generated code.
 * <p><strong>Do not modify!</strong>
 * <p>Please use the <a href="https://docs.web3j.io/command_line.html">web3j command line tools</a>,
 * or the org.web3j.codegen.SolidityFunctionWrapperGenerator in the 
 * <a href="https://github.com/web3j/web3j/tree/master/codegen">codegen module</a> to update.
 *
 * <p>Generated with web3j version 4.3.0.
 */
public class QltProduct extends Contract {
    private static final String BINARY = "6080604052336000806101000a81548173ffffffffffffffffffffffffffffffffffffffff021916908373ffffffffffffffffffffffffffffffffffffffff1602179055503480156200005157600080fd5b5060405162000ff238038062000ff2833981018060405260c08110156200007757600080fd5b8101908080516401000000008111156200009057600080fd5b82810190506020810184811115620000a757600080fd5b8151856001820283011164010000000082111715620000c557600080fd5b50509291906020018051640100000000811115620000e257600080fd5b82810190506020810184811115620000f957600080fd5b81518560018202830111640100000000821117156200011757600080fd5b505092919060200180516401000000008111156200013457600080fd5b828101905060208101848111156200014b57600080fd5b81518560018202830111640100000000821117156200016957600080fd5b505092919060200180516401000000008111156200018657600080fd5b828101905060208101848111156200019d57600080fd5b8151856001820283011164010000000082111715620001bb57600080fd5b50509291906020018051640100000000811115620001d857600080fd5b82810190506020810184811115620001ef57600080fd5b81518560018202830111640100000000821117156200020d57600080fd5b505092919060200180516401000000008111156200022a57600080fd5b828101905060208101848111156200024157600080fd5b81518560018202830111640100000000821117156200025f57600080fd5b505092919050505060016040518060c0016040528088815260200187815260200186815260200185815260200184815260200183815250908060018154018082558091505090600182039060005260206000209060060201600090919290919091506000820151816000019080519060200190620002df92919062000659565b506020820151816001019080519060200190620002fe92919062000659565b5060408201518160020190805190602001906200031d92919062000659565b5060608201518160030190805190602001906200033c92919062000659565b5060808201518160040190805190602001906200035b92919062000659565b5060a08201518160050190805190602001906200037a92919062000659565b505050507f5956d2989e642eba49f3962f9cb8983758769145cbde746a5479b0a1e1003c848686868686866040518080602001806020018060200180602001806020018060200187810387528d818151815260200191508051906020019080838360005b83811015620003fb578082015181840152602081019050620003de565b50505050905090810190601f168015620004295780820380516001836020036101000a031916815260200191505b5087810386528c818151815260200191508051906020019080838360005b838110156200046457808201518184015260208101905062000447565b50505050905090810190601f168015620004925780820380516001836020036101000a031916815260200191505b5087810385528b818151815260200191508051906020019080838360005b83811015620004cd578082015181840152602081019050620004b0565b50505050905090810190601f168015620004fb5780820380516001836020036101000a031916815260200191505b5087810384528a818151815260200191508051906020019080838360005b838110156200053657808201518184015260208101905062000519565b50505050905090810190601f168015620005645780820380516001836020036101000a031916815260200191505b50878103835289818151815260200191508051906020019080838360005b838110156200059f57808201518184015260208101905062000582565b50505050905090810190601f168015620005cd5780820380516001836020036101000a031916815260200191505b50878103825288818151815260200191508051906020019080838360005b8381101562000608578082015181840152602081019050620005eb565b50505050905090810190601f168015620006365780820380516001836020036101000a031916815260200191505b509c5050505050505050505050505060405180910390a150505050505062000708565b828054600181600116156101000203166002900490600052602060002090601f016020900481019282601f106200069c57805160ff1916838001178555620006cd565b82800160010185558215620006cd579182015b82811115620006cc578251825591602001919060010190620006af565b5b509050620006dc9190620006e0565b5090565b6200070591905b8082111562000701576000816000905550600101620006e7565b5090565b90565b6108da80620007186000396000f3fe608060405234801561001057600080fd5b506004361061002b5760003560e01c8063b0fcd11b14610030575b600080fd5b6103dc600480360360c081101561004657600080fd5b810190808035906020019064010000000081111561006357600080fd5b82018360208201111561007557600080fd5b8035906020019184600183028401116401000000008311171561009757600080fd5b91908080601f016020809104026020016040519081016040528093929190818152602001838380828437600081840152601f19601f820116905080830192505050505050509192919290803590602001906401000000008111156100fa57600080fd5b82018360208201111561010c57600080fd5b8035906020019184600183028401116401000000008311171561012e57600080fd5b91908080601f016020809104026020016040519081016040528093929190818152602001838380828437600081840152601f19601f8201169050808301925050505050505091929192908035906020019064010000000081111561019157600080fd5b8201836020820111156101a357600080fd5b803590602001918460018302840111640100000000831117156101c557600080fd5b91908080601f016020809104026020016040519081016040528093929190818152602001838380828437600081840152601f19601f8201169050808301925050505050505091929192908035906020019064010000000081111561022857600080fd5b82018360208201111561023a57600080fd5b8035906020019184600183028401116401000000008311171561025c57600080fd5b91908080601f016020809104026020016040519081016040528093929190818152602001838380828437600081840152601f19601f820116905080830192505050505050509192919290803590602001906401000000008111156102bf57600080fd5b8201836020820111156102d157600080fd5b803590602001918460018302840111640100000000831117156102f357600080fd5b91908080601f016020809104026020016040519081016040528093929190818152602001838380828437600081840152601f19601f8201169050808301925050505050505091929192908035906020019064010000000081111561035657600080fd5b82018360208201111561036857600080fd5b8035906020019184600183028401116401000000008311171561038a57600080fd5b91908080601f016020809104026020016040519081016040528093929190818152602001838380828437600081840152601f19601f8201169050808301925050505050505091929192905050506103de565b005b6000809054906101000a900473ffffffffffffffffffffffffffffffffffffffff168073ffffffffffffffffffffffffffffffffffffffff163373ffffffffffffffffffffffffffffffffffffffff161461043857600080fd5b60016040518060c00160405280898152602001888152602001878152602001868152602001858152602001848152509080600181540180825580915050906001820390600052602060002090600602016000909192909190915060008201518160000190805190602001906104ae929190610809565b5060208201518160010190805190602001906104cb929190610809565b5060408201518160020190805190602001906104e8929190610809565b506060820151816003019080519060200190610505929190610809565b506080820151816004019080519060200190610522929190610809565b5060a082015181600501908051906020019061053f929190610809565b505050507f5956d2989e642eba49f3962f9cb8983758769145cbde746a5479b0a1e1003c848787878787876040518080602001806020018060200180602001806020018060200187810387528d818151815260200191508051906020019080838360005b838110156105be5780820151818401526020810190506105a3565b50505050905090810190601f1680156105eb5780820380516001836020036101000a031916815260200191505b5087810386528c818151815260200191508051906020019080838360005b83811015610624578082015181840152602081019050610609565b50505050905090810190601f1680156106515780820380516001836020036101000a031916815260200191505b5087810385528b818151815260200191508051906020019080838360005b8381101561068a57808201518184015260208101905061066f565b50505050905090810190601f1680156106b75780820380516001836020036101000a031916815260200191505b5087810384528a818151815260200191508051906020019080838360005b838110156106f05780820151818401526020810190506106d5565b50505050905090810190601f16801561071d5780820380516001836020036101000a031916815260200191505b50878103835289818151815260200191508051906020019080838360005b8381101561075657808201518184015260208101905061073b565b50505050905090810190601f1680156107835780820380516001836020036101000a031916815260200191505b50878103825288818151815260200191508051906020019080838360005b838110156107bc5780820151818401526020810190506107a1565b50505050905090810190601f1680156107e95780820380516001836020036101000a031916815260200191505b509c5050505050505050505050505060405180910390a150505050505050565b828054600181600116156101000203166002900490600052602060002090601f016020900481019282601f1061084a57805160ff1916838001178555610878565b82800160010185558215610878579182015b8281111561087757825182559160200191906001019061085c565b5b5090506108859190610889565b5090565b6108ab91905b808211156108a757600081600090555060010161088f565b5090565b9056fea165627a7a7230582036bceca854e8f8cd76bcc352bc8a962317f0f51ccaa646127e9be16a68cb45680029";

    public static final String FUNC_ADDREVIEW = "addReview";

    public static final Event NEWREVIEW_EVENT = new Event("NewReview", 
            Arrays.<TypeReference<?>>asList(new TypeReference<Utf8String>() {}, new TypeReference<Utf8String>() {}, new TypeReference<Utf8String>() {}, new TypeReference<Utf8String>() {}, new TypeReference<Utf8String>() {}, new TypeReference<Utf8String>() {}));
    ;

    @Deprecated
    protected QltProduct(String contractAddress, Web3j web3j, Credentials credentials, BigInteger gasPrice, BigInteger gasLimit) {
        super(BINARY, contractAddress, web3j, credentials, gasPrice, gasLimit);
    }

    protected QltProduct(String contractAddress, Web3j web3j, Credentials credentials, ContractGasProvider contractGasProvider) {
        super(BINARY, contractAddress, web3j, credentials, contractGasProvider);
    }

    @Deprecated
    protected QltProduct(String contractAddress, Web3j web3j, TransactionManager transactionManager, BigInteger gasPrice, BigInteger gasLimit) {
        super(BINARY, contractAddress, web3j, transactionManager, gasPrice, gasLimit);
    }

    protected QltProduct(String contractAddress, Web3j web3j, TransactionManager transactionManager, ContractGasProvider contractGasProvider) {
        super(BINARY, contractAddress, web3j, transactionManager, contractGasProvider);
    }

    public RemoteCall<TransactionReceipt> addReview(String id, String productId, String author, String text, String date, String stars) {
        final Function function = new Function(
                FUNC_ADDREVIEW, 
                Arrays.<Type>asList(new Utf8String(id),
                new Utf8String(productId),
                new Utf8String(author),
                new Utf8String(text),
                new Utf8String(date),
                new Utf8String(stars)),
                Collections.<TypeReference<?>>emptyList());
        return executeRemoteCallTransaction(function);
    }

    public List<NewReviewEventResponse> getNewReviewEvents(TransactionReceipt transactionReceipt) {
        List<EventValuesWithLog> valueList = extractEventParametersWithLog(NEWREVIEW_EVENT, transactionReceipt);
        ArrayList<NewReviewEventResponse> responses = new ArrayList<NewReviewEventResponse>(valueList.size());
        for (EventValuesWithLog eventValues : valueList) {
            NewReviewEventResponse typedResponse = new NewReviewEventResponse();
            typedResponse.log = eventValues.getLog();
            typedResponse.id = (String) eventValues.getNonIndexedValues().get(0).getValue();
            typedResponse.productId = (String) eventValues.getNonIndexedValues().get(1).getValue();
            typedResponse.author = (String) eventValues.getNonIndexedValues().get(2).getValue();
            typedResponse.text = (String) eventValues.getNonIndexedValues().get(3).getValue();
            typedResponse.date = (String) eventValues.getNonIndexedValues().get(4).getValue();
            typedResponse.stars = (String) eventValues.getNonIndexedValues().get(5).getValue();
            responses.add(typedResponse);
        }
        return responses;
    }

    public Flowable<NewReviewEventResponse> newReviewEventFlowable(EthFilter filter) {
        return web3j.ethLogFlowable(filter).map(new io.reactivex.functions.Function<Log, NewReviewEventResponse>() {
            @Override
            public NewReviewEventResponse apply(Log log) {
                EventValuesWithLog eventValues = extractEventParametersWithLog(NEWREVIEW_EVENT, log);
                NewReviewEventResponse typedResponse = new NewReviewEventResponse();
                typedResponse.log = log;
                typedResponse.id = (String) eventValues.getNonIndexedValues().get(0).getValue();
                typedResponse.productId = (String) eventValues.getNonIndexedValues().get(1).getValue();
                typedResponse.author = (String) eventValues.getNonIndexedValues().get(2).getValue();
                typedResponse.text = (String) eventValues.getNonIndexedValues().get(3).getValue();
                typedResponse.date = (String) eventValues.getNonIndexedValues().get(4).getValue();
                typedResponse.stars = (String) eventValues.getNonIndexedValues().get(5).getValue();
                return typedResponse;
            }
        });
    }

    public Flowable<NewReviewEventResponse> newReviewEventFlowable(DefaultBlockParameter startBlock, DefaultBlockParameter endBlock) {
        EthFilter filter = new EthFilter(startBlock, endBlock, getContractAddress());
        filter.addSingleTopic(EventEncoder.encode(NEWREVIEW_EVENT));
        return newReviewEventFlowable(filter);
    }

    @Deprecated
    public static QltProduct load(String contractAddress, Web3j web3j, Credentials credentials, BigInteger gasPrice, BigInteger gasLimit) {
        return new QltProduct(contractAddress, web3j, credentials, gasPrice, gasLimit);
    }

    @Deprecated
    public static QltProduct load(String contractAddress, Web3j web3j, TransactionManager transactionManager, BigInteger gasPrice, BigInteger gasLimit) {
        return new QltProduct(contractAddress, web3j, transactionManager, gasPrice, gasLimit);
    }

    public static QltProduct load(String contractAddress, Web3j web3j, Credentials credentials, ContractGasProvider contractGasProvider) {
        return new QltProduct(contractAddress, web3j, credentials, contractGasProvider);
    }

    public static QltProduct load(String contractAddress, Web3j web3j, TransactionManager transactionManager, ContractGasProvider contractGasProvider) {
        return new QltProduct(contractAddress, web3j, transactionManager, contractGasProvider);
    }

    public static RemoteCall<QltProduct> deploy(Web3j web3j, Credentials credentials, ContractGasProvider contractGasProvider, String _id, String _productId, String _author, String _text, String _date, String _stars) {
        String encodedConstructor = FunctionEncoder.encodeConstructor(Arrays.<Type>asList(new Utf8String(_id),
                new Utf8String(_productId),
                new Utf8String(_author),
                new Utf8String(_text),
                new Utf8String(_date),
                new Utf8String(_stars)));
        return deployRemoteCall(QltProduct.class, web3j, credentials, contractGasProvider, BINARY, encodedConstructor);
    }

    public static RemoteCall<QltProduct> deploy(Web3j web3j, TransactionManager transactionManager, ContractGasProvider contractGasProvider, String _id, String _productId, String _author, String _text, String _date, String _stars) {
        String encodedConstructor = FunctionEncoder.encodeConstructor(Arrays.<Type>asList(new Utf8String(_id),
                new Utf8String(_productId),
                new Utf8String(_author),
                new Utf8String(_text),
                new Utf8String(_date),
                new Utf8String(_stars)));
        return deployRemoteCall(QltProduct.class, web3j, transactionManager, contractGasProvider, BINARY, encodedConstructor);
    }

    @Deprecated
    public static RemoteCall<QltProduct> deploy(Web3j web3j, Credentials credentials, BigInteger gasPrice, BigInteger gasLimit, String _id, String _productId, String _author, String _text, String _date, String _stars) {
        String encodedConstructor = FunctionEncoder.encodeConstructor(Arrays.<Type>asList(new Utf8String(_id),
                new Utf8String(_productId),
                new Utf8String(_author),
                new Utf8String(_text),
                new Utf8String(_date),
                new Utf8String(_stars)));
        return deployRemoteCall(QltProduct.class, web3j, credentials, gasPrice, gasLimit, BINARY, encodedConstructor);
    }

    @Deprecated
    public static RemoteCall<QltProduct> deploy(Web3j web3j, TransactionManager transactionManager, BigInteger gasPrice, BigInteger gasLimit, String _id, String _productId, String _author, String _text, String _date, String _stars) {
        String encodedConstructor = FunctionEncoder.encodeConstructor(Arrays.<Type>asList(new Utf8String(_id),
                new Utf8String(_productId),
                new Utf8String(_author),
                new Utf8String(_text),
                new Utf8String(_date),
                new Utf8String(_stars)));
        return deployRemoteCall(QltProduct.class, web3j, transactionManager, gasPrice, gasLimit, BINARY, encodedConstructor);
    }

    public static class NewReviewEventResponse {
        public Log log;

        public String id;

        public String productId;

        public String author;

        public String text;

        public String date;

        public String stars;
    }
}
