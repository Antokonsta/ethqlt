package com.quality.ethqlt.service;

import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixProperty;
import com.quality.ethqlt.config.EthereumConfig;
import com.quality.ethqlt.domain.ProductContract;
import com.quality.ethqlt.repository.ProductContractRepository;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Resource for working with links to the smart contracts.
 *
 * @author AM_Konstantinov
 */
@Data
@Slf4j
@Service
public class SmartContractLinkService {

    @Autowired
    private ProductContractRepository productContractRepository;

    @Autowired
    private EthereumConfig ethereumConfig;

    @HystrixCommand(
            commandProperties = {
                    @HystrixProperty(name = "execution.isolation.thread.timeoutInMilliseconds", value = "3000")
            },
            threadPoolKey = "contractLinkThreadPool",
            fallbackMethod = "getContractLinkFallback"
    )
    public String getContractLink(Long productId) {
        ProductContract association = productContractRepository.getByProductId(productId);
        String contractLink = null;
        if (association != null && association.getContract() != null) {
            contractLink = ethereumConfig.generateContractLink(association.getContract());
        }
        log.info("getContractLink() for productId = {}; result = {}", productId, contractLink);
        return contractLink;
    }

    public String getContractLinkFallback(Long productId) {
        log.info("getContractLink() making time-out, using fallback");
        return null;
    }
}
