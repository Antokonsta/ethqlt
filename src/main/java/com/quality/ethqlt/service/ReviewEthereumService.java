package com.quality.ethqlt.service;

import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixProperty;
import com.quality.ethqlt.config.EthereumConfig;
import com.quality.ethqlt.contract.QltProduct;
import com.quality.ethqlt.domain.ProductContract;
import com.quality.ethqlt.domain.SaveReviewRequest;
import com.quality.ethqlt.dto.ReviewDto;
import com.quality.ethqlt.mapping.ReviewMapper;
import com.quality.ethqlt.repository.ProductContractRepository;
import com.quality.ethqlt.repository.SaveReviewRequestRepository;
import lombok.Data;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.web3j.crypto.Credentials;
import org.web3j.protocol.Web3j;
import org.web3j.protocol.core.methods.response.TransactionReceipt;
import org.web3j.tx.gas.ContractGasProvider;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import static com.quality.ethqlt.util.ValidationUtil.nullCheck;

/**
 * Resource for working with quality888's reviews in ethereum.
 *
 * @author AM_Konstantinov
 */
@Data
@Service
@Slf4j
@RequiredArgsConstructor
public class ReviewEthereumService {

    private final EthereumConfig ethereumConfig;
    private final ProductContractRepository productContractRepository;
    private final SaveReviewRequestRepository saveReviewRequestRepository;
    private final ReviewMapper reviewMapper;

    private static final DateFormat FORMATTER = new SimpleDateFormat("dd-MM-yyyy HH:mm");

    @HystrixCommand(
            commandProperties = {
                    @HystrixProperty(name = "execution.isolation.thread.timeoutInMilliseconds", value = "3000")
            },
            threadPoolKey = "saveReviewThreadPool"
    )
    public void saveReview(@NonNull ReviewDto reviewDto) throws Exception {
        log.info("Receive SaveReview request = {}", reviewDto);
        nullCheck(reviewDto.getProductId(), "ProductId is Null");
        SaveReviewRequest saveReviewRequest = reviewMapper.toSaveRequest(reviewDto);
        saveReviewRequestRepository.save(saveReviewRequest);

        log.info("Review with id = {} was added in queue for processing", reviewDto.getId());
    }

    @Scheduled(fixedDelay = 10000)
    public void processSaveReviewRequests() throws Exception {
        log.debug("start processing SaveReview Requests");
        List<SaveReviewRequest> top3ByProcessedFalse = saveReviewRequestRepository.findTop3ByProcessedFalse();
        for (SaveReviewRequest saveRequest : top3ByProcessedFalse) {
            processSaveReviewRequests(saveRequest);
        }
    }

    @Scheduled(fixedDelay = 3600000)//каждый час
    @Transactional
    public void deleteProcessed() {
        log.info("start deleting processed SaveReview Requests");
        saveReviewRequestRepository.deleteByProcessedTrue();
    }

    @Transactional
    @HystrixCommand(
            commandProperties = {
                    @HystrixProperty(name = "execution.isolation.thread.timeoutInMilliseconds", value = "120000")
            },
            threadPoolKey = "saveReviewThreadPool"
    )
    public void processSaveReviewRequests(SaveReviewRequest saveRequest) throws Exception {
        ProductContract productContract = productContractRepository.getByProductId(saveRequest.getProductId());
        if (productContract == null) {
            String contract = createProductContract(saveRequest);
            productContractRepository.save(ProductContract.builder()
                    .productId(saveRequest.getProductId())
                    .contract(contract)
                    .build());
        } else {
            saveReviewInExistingContract(productContract.getContract(), saveRequest);
        }

        saveRequest.setProcessed(true);
        saveReviewRequestRepository.save(saveRequest);
    }


    private String createProductContract(SaveReviewRequest saveRequest) throws Exception {
        Web3j web3j = ethereumConfig.getEthereumConnection();
        Credentials credentials = ethereumConfig.getCredentials();
        ContractGasProvider gasProvider = ethereumConfig.getGasProvider();

        QltProduct contract = QltProduct.deploy(
                web3j,
                credentials,
                gasProvider,
                String.valueOf(saveRequest.getReviewId()),
                String.valueOf(saveRequest.getProductId()),
                saveRequest.getAuthor(),
                saveRequest.getText(),
                saveRequest.getDate() != null ? FORMATTER.format(new Date(saveRequest.getDate())) : "",
                String.valueOf(saveRequest.getStars()))
                .send();

        nullCheck(contract, "Ethereum didn't create a contract for review: " + saveRequest.toString());
        nullCheck(contract.getContractAddress(), "Ethereum returned NULL contract address");

        log.info("Product Contract was created with contract address = {} for saveReviewRequest with id = {}",
                contract.getContractAddress(), saveRequest.getId());
        return contract.getContractAddress();
    }

    private void saveReviewInExistingContract(String contract, SaveReviewRequest saveRequest) throws Exception {
        Web3j web3j = ethereumConfig.getEthereumConnection();
        Credentials credentials = ethereumConfig.getCredentials();
        ContractGasProvider gasProvider = ethereumConfig.getGasProvider();

        QltProduct load = QltProduct.load(contract, web3j, credentials, gasProvider);
        nullCheck(load, "Load nothing by contract addresss : " + contract);

        TransactionReceipt transaction = load.addReview(
                String.valueOf(saveRequest.getReviewId()),
                String.valueOf(saveRequest.getProductId()),
                saveRequest.getAuthor(),
                saveRequest.getText(),
                saveRequest.getDate() != null ? FORMATTER.format(new Date(saveRequest.getDate())) : "",
                String.valueOf(saveRequest.getStars()))
                .send();
        nullCheck(transaction, "transaction is null for saveReviewRequest with id : " + saveRequest.getId());
        log.info("Saved review in existing contract = {} for saveReviewRequest with id = {}", contract, saveRequest.getId());
    }
}
