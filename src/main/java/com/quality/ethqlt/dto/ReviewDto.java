package com.quality.ethqlt.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ReviewDto {

    @ApiModelProperty(value = "Id of review", example = "1")
    private Long id;

    @ApiModelProperty(value = "Product id", example = "5")
    private Long productId;

    @ApiModelProperty(value = "Reviews's author", example = "Emma Watson")
    private String author;

    @ApiModelProperty(value = "Reviews's text", example = "It is a product with a good quality")
    private String text;

    @ApiModelProperty(value = "Reviews's date in milliseconds")
    private Long date;

    @ApiModelProperty(value = "Reviews's start", example = "3")
    private int stars;
}
