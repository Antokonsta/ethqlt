package com.quality.ethqlt.util;

import lombok.experimental.UtilityClass;

import java.util.Objects;


/**
 * Class for working with validation.
 *
 * @author AM_Konstantinov
 */
@UtilityClass
public class ValidationUtil {

    public static void nullCheck(Object field, String message) {
        if (Objects.isNull(field)) {
            throw new NullPointerException(message);
        }
    }
}
