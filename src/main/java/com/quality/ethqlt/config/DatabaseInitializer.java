package com.quality.ethqlt.config;

import com.quality.ethqlt.domain.Parameter;
import com.quality.ethqlt.enums.ParameterInitialValues;
import com.quality.ethqlt.repository.ParameterRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

import java.util.Arrays;


@Component
public class DatabaseInitializer implements ApplicationRunner {

    @Autowired
    private ParameterRepository parameterRepository;

    @Override
    public void run(ApplicationArguments args) throws Exception {
        parameterRepository.deleteAll();
        Arrays.stream(ParameterInitialValues.values())
                .forEach(param -> parameterRepository.save(Parameter.builder()
                        .code(param.getCode())
                        .value(param.getValue())
                        .build()));

    }
}
