package com.quality.ethqlt.config;

import com.quality.ethqlt.domain.Parameter;
import com.quality.ethqlt.repository.ParameterRepository;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.web3j.crypto.Credentials;
import org.web3j.crypto.WalletUtils;
import org.web3j.protocol.Web3j;
import org.web3j.protocol.http.HttpService;
import org.web3j.tx.gas.ContractGasProvider;
import org.web3j.tx.gas.StaticGasProvider;

import javax.annotation.PostConstruct;
import java.io.*;
import java.math.BigInteger;
import java.net.URL;

import static com.quality.ethqlt.enums.ParameterInitialValues.GAS_LIMIT_PARAM_NAME;
import static com.quality.ethqlt.enums.ParameterInitialValues.GAS_PRICE_PARAM_NAME;


@Data
@Slf4j
@Component
public class EthereumConfig {

    private Credentials credentials;

    @Value("${ethereum.wallet.password}")
    private String password;

    @Value("${ethereum.wallet.path}")
    private String pathToWallet;

    @Value("${ethereum.node.address}")
    private String ethereumNodeAddress;

    @Value("${ethereum.link.prefix}")
    private String linkPrefix;

    @Value("${ethereum.link.postfix}")
    private String linkPostfix;

    @Autowired
    private ParameterRepository parameterRepository;


    @PostConstruct
    private void startup() {
        log.info("Start opening wallet file as postConstruct");

        File file = null;
        URL res = getClass().getResource(pathToWallet);
        if (res.toString().startsWith("jar:")) {
            try {
                InputStream input = getClass().getResourceAsStream(pathToWallet);
                file = File.createTempFile("tempfile", ".tmp");
                OutputStream out = new FileOutputStream(file);
                int read;
                byte[] bytes = new byte[1024];

                while ((read = input.read(bytes)) != -1) {
                    out.write(bytes, 0, read);
                }
                file.deleteOnExit();
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        } else {
            //this will probably work in your IDE, but not from a JAR
            file = new File(res.getFile());
        }

        if (file == null || !file.exists()) {
            throw new RuntimeException("Error: File " + file + " not found!");
        }
        try {
            credentials = WalletUtils.loadCredentials(password, file.getPath());
            log.info("Wallet was successfully opened");
        } catch (Exception e) {
            log.error("Exceptions was occurred while opening wallet file", e);
        }
    }

    public Web3j getEthereumConnection() {
        HttpService httpService = new HttpService(ethereumNodeAddress);
        return Web3j.build(httpService);
    }

    public ContractGasProvider getGasProvider() {
        return new StaticGasProvider(getGasPrice(), getGasLimit());
    }

    public BigInteger getGasPrice() {
        Parameter gasPriceParam = parameterRepository.getByCode(GAS_PRICE_PARAM_NAME.getCode());
        String valueString = gasPriceParam.getValue();
        return new BigInteger(valueString);
    }

    public BigInteger getGasLimit() {
        Parameter gasLimitParam = parameterRepository.getByCode(GAS_LIMIT_PARAM_NAME.getCode());
        String valueString = gasLimitParam.getValue();
        return new BigInteger(valueString);
    }

    public String generateContractLink(String contractAddress) {
        return linkPrefix + contractAddress + linkPostfix;
    }

}

