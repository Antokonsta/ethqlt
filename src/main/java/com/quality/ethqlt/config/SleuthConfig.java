package com.quality.ethqlt.config;

import brave.sampler.Sampler;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import static brave.sampler.Sampler.ALWAYS_SAMPLE;

@Configuration

public class SleuthConfig {
    @Bean
    public Sampler defaultSampler() {
        return ALWAYS_SAMPLE;
    }
}
