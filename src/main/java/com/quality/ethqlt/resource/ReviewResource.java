package com.quality.ethqlt.resource;

import com.quality.ethqlt.dto.ExceptionDto;
import com.quality.ethqlt.dto.ReviewDto;
import com.quality.ethqlt.service.ReviewEthereumService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

/**
 * Resource for working with Quality888's reviews.
 *
 * @author AM_Konstantinov
 */
@Api(tags = "Resource for working with Quality888's reviews")
@RestController
@RequestMapping("/review")
@Data
@Slf4j
public class ReviewResource {

    @Autowired
    private ReviewEthereumService reviewEthereumService;


    @ApiOperation("Save review from Quality888 service")
    @ApiResponses({
            @ApiResponse(code = 200, message = "SaveReview request was saved in queue"),
            @ApiResponse(code = 400, message = "Something went wrong. See the exception")
    })
    @PostMapping(value = "/save", produces = MediaType.APPLICATION_JSON_VALUE)
    public @ResponseBody
    ResponseEntity saveReviewRequest(@RequestBody ReviewDto reviewDto) {
        try {
            reviewEthereumService.saveReview(reviewDto);
            return ResponseEntity.ok().build();
        } catch (Exception e) {
            log.error("Exception occurred while saving review", e);
            return ResponseEntity.badRequest()
                    .body(ExceptionDto.builder()
                            .message(e.getMessage())
                            .build());
        }
    }
}
