package com.quality.ethqlt.resource;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * Heart beat Resource.
 *
 * @author AM_Konstantinov
 */
@Api(tags = "Check liveliness of the application")
@RestController
@RequestMapping("/heart")
public class HeartbeatResource {

    @ApiOperation("Return heartbeat. I'm alive - if it's working")
    @GetMapping(value = "/beat", produces = MediaType.APPLICATION_JSON_VALUE)
    public @ResponseBody String helloWorld() {
        return "I'M ALIVE";
    }
}
