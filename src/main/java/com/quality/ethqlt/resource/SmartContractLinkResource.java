package com.quality.ethqlt.resource;

import com.quality.ethqlt.dto.LinkDto;
import com.quality.ethqlt.service.SmartContractLinkService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

/**
 * Resource for working with links to the smart contracts.
 *
 * @author AM_Konstantinov
 */
@Api(tags = "Resource for working with links to the smart contracts")
@RestController
@RequestMapping("/contracts")
@Data
public class SmartContractLinkResource {

    @Autowired
    private SmartContractLinkService smartContractLinkService;

    @ApiOperation("Return link to the contract by product id")
    @ApiResponses({
            @ApiResponse(code = 200, message = "OK. Return link to the contract"),
            @ApiResponse(code = 404, message = "There is no contract yet for the product")
    })
    @GetMapping(value = "/link/{productId}", produces = MediaType.APPLICATION_JSON_VALUE)
    public @ResponseBody
    ResponseEntity<LinkDto> getContractLink(@PathVariable Long productId) {
        String contractLink = smartContractLinkService.getContractLink(productId);
        return contractLink != null
                ? ResponseEntity.ok(LinkDto.builder().smartContractLink(contractLink).build())
                : ResponseEntity.notFound().build();
    }
}
