package com.quality.ethqlt.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum ParameterInitialValues {
    GAS_PRICE_PARAM_NAME("ethereum.node.gasPrice", "22000000000"),

    GAS_LIMIT_PARAM_NAME("ethereum.node.gasLimit", "4300000");

    private String code;

    private String value;

}
