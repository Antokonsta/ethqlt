package com.quality.ethqlt.repository;

import com.quality.ethqlt.domain.SaveReviewRequest;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface SaveReviewRequestRepository extends CrudRepository<SaveReviewRequest, Long> {
    List<SaveReviewRequest> findTop3ByProcessedFalse();

    void deleteByProcessedTrue();
}
