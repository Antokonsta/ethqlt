package com.quality.ethqlt.repository;

import com.quality.ethqlt.domain.ProductContract;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ProductContractRepository extends CrudRepository<ProductContract, Integer> {

    ProductContract getByProductId(Long productId);
}
