package com.quality.ethqlt.repository;

import com.quality.ethqlt.domain.Parameter;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ParameterRepository extends CrudRepository<Parameter, Integer> {

    Parameter getByCode(String code);
}
