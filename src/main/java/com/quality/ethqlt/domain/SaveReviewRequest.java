package com.quality.ethqlt.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;


@Entity
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "save_review_request")
public class SaveReviewRequest {

    @Id
    @GeneratedValue
    private Long id;

    @Column(name = "reviewId", nullable = false)
    private Long reviewId;

    @Column(name = "productId", nullable = false)
    private Long productId;

    @Column(name = "author")
    private String author;

    @Column(name = "text")
    private String text;

    @Column(name = "date")
    private Long date;

    @Column(name = "stars")
    private int stars;

    @Column(name = "processed")
    private boolean processed = false;
}
