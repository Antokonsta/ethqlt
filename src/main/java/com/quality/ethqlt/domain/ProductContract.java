package com.quality.ethqlt.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;


/**
 * Table with relation product:contract.
 *
 * @author AM_Konstantinov
 */
@Entity
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "product_contract_relation")
public class ProductContract {

    @Id
    @GeneratedValue
    private Integer id;

    @Column(name = "productId", nullable = false)
    private Long productId;

    @Column(name = "contract", nullable = false)
    private String contract;
}
