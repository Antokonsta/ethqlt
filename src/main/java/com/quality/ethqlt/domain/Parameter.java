package com.quality.ethqlt.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;


/**
 * Table with variable parameters.
 *
 * @author AM_Konstantinov
 */
@Entity
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "variable_parameters")
public class Parameter {

    @Id
    @GeneratedValue
    private Integer id;

    @Column(name = "code", nullable = false)
    private String code;

    @Column(name = "value", nullable = false)
    private String value;
}
