package com.quality.ethqlt.mapping;

import com.quality.ethqlt.domain.SaveReviewRequest;
import com.quality.ethqlt.dto.ReviewDto;
import org.springframework.stereotype.Component;

@Component
public class ReviewMapper {

    public SaveReviewRequest toSaveRequest(ReviewDto reviewDto){
        return SaveReviewRequest.builder()
                .reviewId(reviewDto.getId())
                .productId(reviewDto.getProductId())
                .author(reviewDto.getAuthor())
                .text(reviewDto.getText())
                .date(reviewDto.getDate())
                .stars(reviewDto.getStars())
                .processed(false)
                .build();
    }
}
