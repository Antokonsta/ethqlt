package com.quality.ethqlt;

import com.quality.ethqlt.dto.ReviewDto;
import com.quality.ethqlt.service.ReviewEthereumService;
import com.quality.ethqlt.service.SmartContractLinkService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Date;

import static junit.framework.TestCase.assertNotNull;
import static junit.framework.TestCase.assertNull;

@RunWith(SpringRunner.class)
@SpringBootTest
@Disabled
@TestPropertySource("/application-test.properties")
class EthqltFullScenarioTest {

    @Autowired
    private SmartContractLinkService smartContractLinkService;

    @Autowired
    private ReviewEthereumService reviewEthereumService;

    private ReviewDto initialReview;

    private ReviewDto updateReview;

    private static final Long PRODUCT_ID = 1L;

    @BeforeEach
    public void setUp() {
        initialReview = new ReviewDto();
        initialReview.setId(1L);
        initialReview.setProductId(PRODUCT_ID);
        initialReview.setAuthor("John Snow");
        initialReview.setDate(new Date().getTime());
        initialReview.setStars(5);
        initialReview.setText("very good product");

        updateReview = new ReviewDto();
        updateReview.setId(2L);
        updateReview.setProductId(PRODUCT_ID);
        updateReview.setAuthor("Mike Miller");
        updateReview.setDate(new Date().getTime());
        updateReview.setStars(8);
        updateReview.setText("excellent quality");

    }

    @Test
    void fullScenario() throws Exception {
        assertNull(smartContractLinkService.getContractLink(PRODUCT_ID));
        reviewEthereumService.saveReview(initialReview);
        reviewEthereumService.saveReview(updateReview);

        Thread.sleep(150000);
        //тут сработает таска на обработку запросов на сохранение ревью
        assertNotNull(smartContractLinkService.getContractLink(PRODUCT_ID));
    }

}
